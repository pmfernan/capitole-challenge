package com.ferdonof.capitole.port.impl;

import com.ferdonof.capitole.business.domains.RequestPriceBO;
import com.ferdonof.capitole.business.domains.ResponsePriceBO;
import com.ferdonof.capitole.business.exceptions.PriceNotFoundException;
import com.ferdonof.capitole.business.exceptions.UnhandledPortException;
import com.ferdonof.capitole.data_access.PriceRepository;
import com.ferdonof.capitole.data_access.entities.Price;
import com.ferdonof.capitole.port.PriceEntityPort;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class PriceEntityPortImpl implements PriceEntityPort {

    private final PriceRepository priceRepository;

    public PriceEntityPortImpl(PriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }

    @Override
    public ResponsePriceBO getPriceByBrandIdProductIdAndDate(RequestPriceBO requestPriceBO) {


        try {
            Price price = priceRepository.findTopByBrandIdAndProductIdAndStartDateLessThanEqualAndEndDateGreaterThanEqualOrderByPriorityDesc(
                    requestPriceBO.getBrandId(),
                    requestPriceBO.getProductId(),
                    requestPriceBO.getDate(),
                    requestPriceBO.getDate()
            ).orElseThrow(() -> new EntityNotFoundException(
                    "Brand ID: " + requestPriceBO.getBrandId() + ", " +
                    "Product ID: "+ requestPriceBO.getProductId() + " and " +
                    "Date: "+ requestPriceBO.getDate().toString()
            ));

            return ResponsePriceBO.builder()
                    .brandId(price.getBrandId())
                    .productId(price.getProductId())
                    .startDate(price.getStartDate())
                    .endDate(price.getEndDate())
                    .price(price.getPrice())
                    .rateId(price.getPriceListId())
                    .build();
        } catch (EntityNotFoundException e){
            throw new PriceNotFoundException("Can't find price for " + e.getLocalizedMessage());
        } catch (Exception e) {
            throw new UnhandledPortException(e.getLocalizedMessage());
        }
    }
}
