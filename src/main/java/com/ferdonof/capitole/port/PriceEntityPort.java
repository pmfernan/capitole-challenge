package com.ferdonof.capitole.port;

import com.ferdonof.capitole.business.domains.RequestPriceBO;
import com.ferdonof.capitole.business.domains.ResponsePriceBO;

public interface PriceEntityPort {
    public ResponsePriceBO getPriceByBrandIdProductIdAndDate(RequestPriceBO requestPriceBO);
}
