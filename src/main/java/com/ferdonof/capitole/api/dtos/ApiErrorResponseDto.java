package com.ferdonof.capitole.api.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ApiErrorResponseDto {
    private final String code;
    private final String message;
}
