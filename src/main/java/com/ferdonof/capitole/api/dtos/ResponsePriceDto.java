package com.ferdonof.capitole.api.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Builder
@Getter
public class ResponsePriceDto {
    @JsonProperty("product_id")
    private final Long productId;
    @JsonProperty("brand_id")
    private final Long brandId;
    @JsonProperty("rate_id")
    private final Integer rateId;
    @JsonProperty("start_date")
    private final LocalDateTime startDate;
    @JsonProperty("end_date")
    private final LocalDateTime endDate;
    private final BigDecimal price;

}
