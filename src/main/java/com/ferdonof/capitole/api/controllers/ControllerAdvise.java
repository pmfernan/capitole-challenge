package com.ferdonof.capitole.api.controllers;

import com.ferdonof.capitole.api.dtos.ApiErrorResponseDto;
import com.ferdonof.capitole.business.exceptions.PriceNotFoundException;
import com.ferdonof.capitole.business.exceptions.UnhandledPortException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerAdvise extends ResponseEntityExceptionHandler {

    @ExceptionHandler(PriceNotFoundException.class)
    public ResponseEntity<ApiErrorResponseDto> onPriceNotFoundException(RuntimeException e){
        return new ResponseEntity<ApiErrorResponseDto>(new ApiErrorResponseDto("entity.not.found", e.getLocalizedMessage()),
                null,
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UnhandledPortException.class)
    public ResponseEntity<ApiErrorResponseDto> onUnhandledPortException(RuntimeException e, WebRequest request){
        return new ResponseEntity<ApiErrorResponseDto>( new ApiErrorResponseDto("unhandled.port.exception", e.getLocalizedMessage()),
                null,
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
