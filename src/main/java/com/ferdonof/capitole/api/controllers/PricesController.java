package com.ferdonof.capitole.api.controllers;

import com.ferdonof.capitole.api.dtos.ResponsePriceDto;
import com.ferdonof.capitole.business.domains.ResponsePriceBO;
import com.ferdonof.capitole.business.usecases.GetPriceUseCase;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/api/prices")
public class PricesController {

    private final GetPriceUseCase getPriceUseCase;

    public PricesController(GetPriceUseCase getPriceUseCase) {
        this.getPriceUseCase = getPriceUseCase;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponsePriceDto getPrice(@RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime date,
                                     @RequestParam("brand_id") Long brandId, @RequestParam("product_id") Long productId) {

        ResponsePriceBO response = getPriceUseCase.execute(date, brandId, productId);

        return ResponsePriceDto.builder()
                .brandId(response.getBrandId())
                .productId(response.getProductId())
                .rateId(response.getRateId())
                .startDate(response.getStartDate())
                .endDate(response.getEndDate())
                .price(response.getPrice())
                .build();
    }

}
