package com.ferdonof.capitole.data_access.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Currency;

@Entity
@Table(name = "PRICE")
@Data
public class Price implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "brand_id")
    private Long brandId;
    @Column(name = "product_id")
    private Long productId;
    @Column(name = "price_list_id")
    private Integer priceListId;
    @Column(name = "start_date")
    private LocalDateTime startDate;
    @Column(name = "end_date")
    private LocalDateTime endDate;
    private BigDecimal price;
    private Integer priority;
    @Column(name = "curr")
    private Currency currency;
}
