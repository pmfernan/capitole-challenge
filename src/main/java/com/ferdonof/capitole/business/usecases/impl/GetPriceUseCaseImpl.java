package com.ferdonof.capitole.business.usecases.impl;

import com.ferdonof.capitole.business.domains.RequestPriceBO;
import com.ferdonof.capitole.business.domains.ResponsePriceBO;
import com.ferdonof.capitole.business.usecases.GetPriceUseCase;
import com.ferdonof.capitole.port.PriceEntityPort;

import java.time.LocalDateTime;

public class GetPriceUseCaseImpl implements GetPriceUseCase {

    private final PriceEntityPort priceEntityPort;

    public GetPriceUseCaseImpl(PriceEntityPort priceEntityPort) {
        this.priceEntityPort = priceEntityPort;
    }

    @Override
    public ResponsePriceBO execute(LocalDateTime date, Long brandId, Long productId) {
        return priceEntityPort.getPriceByBrandIdProductIdAndDate(new RequestPriceBO(brandId, productId, date ));
    }
}
