package com.ferdonof.capitole.business.usecases;

import com.ferdonof.capitole.business.domains.ResponsePriceBO;

import java.time.LocalDateTime;

public interface GetPriceUseCase {
    ResponsePriceBO execute(LocalDateTime date, Long branId, Long productId);
}
