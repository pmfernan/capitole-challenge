package com.ferdonof.capitole.business.domains;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;


@Getter
@AllArgsConstructor
public class RequestPriceBO {
    private final Long brandId;
    private final Long productId;
    private final LocalDateTime date;
}
