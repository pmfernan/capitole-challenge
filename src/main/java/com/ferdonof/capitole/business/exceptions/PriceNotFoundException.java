package com.ferdonof.capitole.business.exceptions;

public class PriceNotFoundException extends RuntimeException{
    public PriceNotFoundException(String message){
        super(message);
    }
}
