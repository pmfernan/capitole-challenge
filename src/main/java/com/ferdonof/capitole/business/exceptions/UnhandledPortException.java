package com.ferdonof.capitole.business.exceptions;

public class UnhandledPortException extends RuntimeException{
        public UnhandledPortException(String message){
            super(message);
        }
}
