package com.ferdonof.capitole.configuration;

import com.ferdonof.capitole.business.usecases.GetPriceUseCase;
import com.ferdonof.capitole.business.usecases.impl.GetPriceUseCaseImpl;
import com.ferdonof.capitole.port.PriceEntityPort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UsecaseConfiguration {

    @Bean
    public GetPriceUseCase getGetPriceUseCase(PriceEntityPort priceEntityPort){
        return new GetPriceUseCaseImpl(priceEntityPort);
    }
}
