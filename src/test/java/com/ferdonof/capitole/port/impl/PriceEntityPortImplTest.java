package com.ferdonof.capitole.port.impl;

import com.ferdonof.capitole.business.domains.RequestPriceBO;
import com.ferdonof.capitole.business.domains.ResponsePriceBO;
import com.ferdonof.capitole.business.exceptions.PriceNotFoundException;
import com.ferdonof.capitole.business.exceptions.UnhandledPortException;
import com.ferdonof.capitole.data_access.PriceRepository;
import com.ferdonof.capitole.data_access.entities.Price;
import com.ferdonof.capitole.port.PriceEntityPort;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Currency;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

class PriceEntityPortImplTest {

    private PriceRepository priceRepository = mock(PriceRepository.class);
    private PriceEntityPort port = new PriceEntityPortImpl(priceRepository);
    private LocalDateTime now = LocalDateTime.now();

    @Test
    void whenEntityExistsThenReturnResponsePriceBo() {
        when(priceRepository.findTopByBrandIdAndProductIdAndStartDateLessThanEqualAndEndDateGreaterThanEqualOrderByPriorityDesc(anyLong(), anyLong(), any(), any()))
                .thenReturn(Optional.ofNullable(getPrice()));
        ResponsePriceBO returned = port.getPriceByBrandIdProductIdAndDate(new RequestPriceBO(1l, 35455l, LocalDateTime.now()));

        verify(priceRepository, times(1)).findTopByBrandIdAndProductIdAndStartDateLessThanEqualAndEndDateGreaterThanEqualOrderByPriorityDesc(anyLong(), anyLong(), any(), any());

        Assertions.assertThat(returned).usingRecursiveComparison().isEqualTo(getResponsePriceBo());
    }


    @Test
    void whenEntityNotExistsThenThrowPriceNotFoundException()  {
        try {
            when(priceRepository.findTopByBrandIdAndProductIdAndStartDateLessThanEqualAndEndDateGreaterThanEqualOrderByPriorityDesc(1l, 35455l, now, now))
                    .thenThrow(new EntityNotFoundException(null));

            port.getPriceByBrandIdProductIdAndDate(new RequestPriceBO(1l,35455l,now));

        } catch (Exception e){
            Assertions.assertThat(e).isInstanceOf(PriceNotFoundException.class);
        }

        verify(priceRepository, times(1))
                .findTopByBrandIdAndProductIdAndStartDateLessThanEqualAndEndDateGreaterThanEqualOrderByPriorityDesc(1l, 35455l, now, now);

    }

    @Test
    void whenEntityNotExistsThenThrowUnhandledPortException()  {
        try {
            when(priceRepository.findTopByBrandIdAndProductIdAndStartDateLessThanEqualAndEndDateGreaterThanEqualOrderByPriorityDesc(1l, 35455l, now, now))
                    .thenThrow(new RuntimeException());

            port.getPriceByBrandIdProductIdAndDate(new RequestPriceBO(1l,35455l,now));

        } catch (Exception e){
            Assertions.assertThat(e).isInstanceOf(UnhandledPortException.class);
        }

        verify(priceRepository, times(1))
                .findTopByBrandIdAndProductIdAndStartDateLessThanEqualAndEndDateGreaterThanEqualOrderByPriorityDesc(1l, 35455l, now, now);

    }


    public Price getPrice(){
        Price price = new Price();
        price.setId(1l);
        price.setPriority(0);
        price.setBrandId(1l);
        price.setPriceListId(1);
        price.setProductId(35455l);
        price.setPrice(BigDecimal.TEN);
        price.setEndDate(now);
        price.setStartDate(now);
        price.setCurrency(Currency.getInstance("EUR"));
        return price;
    }

    public ResponsePriceBO getResponsePriceBo(){
        return ResponsePriceBO
                .builder()
                .brandId(1l)
                .productId(35455l)
                .startDate(now)
                .endDate(now)
                .rateId(1)
                .price(BigDecimal.TEN)
                .build();

    }

}