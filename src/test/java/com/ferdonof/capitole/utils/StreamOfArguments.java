package com.ferdonof.capitole.utils;

import com.ferdonof.capitole.api.dtos.ResponsePriceDto;
import org.junit.jupiter.params.provider.Arguments;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.stream.Stream;

public class StreamOfArguments {
    public static Stream<Arguments> get(){
        return Stream.of(
                Arguments.of(LocalDateTime.of(2020,06, 14,10,0,0), 1l, 35455l, getFirstRow()),

                Arguments.of(LocalDateTime.of(2020,06, 14,16,0,0), 1l, 35455l, getSecondRow()),

                Arguments.of(LocalDateTime.of(2020,06, 14,21,0,0), 1l, 35455l, getFirstRow()),

                Arguments.of(LocalDateTime.of(2020,06, 15,10,0,0), 1l, 35455l, getThirdRow()),

                Arguments.of(LocalDateTime.of(2020,06, 16,21,0,0), 1l, 35455l, getFourthRow())
        );
    }

    private static ResponsePriceDto getFourthRow() {
        return ResponsePriceDto.builder().brandId(1l)
                .productId(35455l).rateId(4).startDate(LocalDateTime.of(2020, 06, 15, 16, 0, 0, 0))
                .endDate(LocalDateTime.of(2020, 12, 31, 23, 59, 59, 0)).price(BigDecimal.valueOf(38.95d)).build();
    }

    private static ResponsePriceDto getThirdRow() {
        return ResponsePriceDto.builder().brandId(1l)
                .productId(35455l).rateId(3).startDate(LocalDateTime.of(2020, 06, 15, 0, 0, 0, 0))
                .endDate(LocalDateTime.of(2020, 06, 15, 11, 0, 0, 0)).price(BigDecimal.valueOf(30.50d)).build();
    }

    private static ResponsePriceDto getSecondRow() {
        return ResponsePriceDto.builder().brandId(1l)
                .productId(35455l).rateId(2).startDate(LocalDateTime.of(2020, 06, 14, 15, 0, 0, 0))
                .endDate(LocalDateTime.of(2020, 06, 14, 18, 30, 0, 0)).price(BigDecimal.valueOf(25.45d)).build();
    }

    private static ResponsePriceDto getFirstRow() {
        return ResponsePriceDto.builder().brandId(1l)
                .productId(35455l).rateId(1).startDate(LocalDateTime.of(2020, 06, 14, 0, 0, 0, 0))
                .endDate(LocalDateTime.of(2020, 12, 31, 23, 59, 59, 0)).price(BigDecimal.valueOf(35.50d)).build();
    }
}
