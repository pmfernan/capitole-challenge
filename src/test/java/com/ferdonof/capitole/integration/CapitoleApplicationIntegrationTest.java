package com.ferdonof.capitole.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ferdonof.capitole.api.controllers.PricesController;
import com.ferdonof.capitole.api.dtos.ResponsePriceDto;
import com.ferdonof.capitole.business.exceptions.PriceNotFoundException;
import com.ferdonof.capitole.business.usecases.GetPriceUseCase;
import com.ferdonof.capitole.business.usecases.impl.GetPriceUseCaseImpl;
import com.ferdonof.capitole.data_access.PriceRepository;
import com.ferdonof.capitole.port.PriceEntityPort;
import com.ferdonof.capitole.utils.StreamOfArguments;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.sql.DataSource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class CapitoleApplicationIntegrationTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private PriceRepository priceRepository;
    @Autowired
    private PriceEntityPort priceEntityPort;
    private GetPriceUseCase getPriceUseCase;
    @Autowired @Lazy
    private PricesController pricesController;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DataSource dataSource;

    @BeforeEach
    void setup(){
        getPriceUseCase = new GetPriceUseCaseImpl(priceEntityPort);
        pricesController = new PricesController(getPriceUseCase);
    }

    @ParameterizedTest
    @MethodSource("testArguments")
    void whenReceiveValidParametersThenReturnResponsePriceDto(LocalDateTime date, Long brandId, Long productId, ResponsePriceDto response) throws Exception {
            
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get("/api/prices")
                .param("product_id",productId.toString())
                .param("brand_id",brandId.toString())
                .param("date", date.toString())
                .accept(MediaType.APPLICATION_JSON));

        result.andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.product_id").value(response.getProductId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.brand_id").value(response.getBrandId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.rate_id").value(response.getRateId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.start_date").value(response.getStartDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.end_date").value(response.getEndDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price").value(response.getPrice()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

    }


    @Test
    void whenIsMissingProductIdShouldReturn404BadRequest() throws Exception {
        
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get("/api/prices")
                .param("brand_id","1")
                .param("date", LocalDateTime.of(2020,06, 14,10,0,0).toString())
                .accept(MediaType.APPLICATION_JSON));

        result.andDo(print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

    }

    @Test
    void whenIsMissingBrandIdShouldReturn404BadRequest() throws Exception {
        
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get("/api/prices")
                .param("product_id","35455")
                .param("date", LocalDateTime.of(2020,06, 14,10,0,0).toString())
                .accept(MediaType.APPLICATION_JSON));

        result.andDo(print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

    }

    @Test
    void whenIsMissingDateShouldReturn404BadRequest() throws Exception {
        
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get("/api/prices")
                .param("product_id","35455")
                .param("brand_id","1")
                .accept(MediaType.APPLICATION_JSON));

        result.andDo(print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();

    }

    @Test
    void whenNoEntityIsFoundShouldReturnNotFound() throws Exception {
        
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get("/api/prices")
                .param("product_id","35455")
                .param("brand_id","1")
                .param("date", LocalDateTime.of(2021,06, 14,10,0,0).toString())
                .accept(MediaType.APPLICATION_JSON));

        result.andDo(print())
                .andExpect(result1 -> Assertions.assertTrue(result1.getResolvedException() instanceof PriceNotFoundException))
                .andExpect(result1 -> Assertions.assertTrue(result1.getResolvedException().getLocalizedMessage().contains("Can't find price")))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andReturn();
    }

    private static Stream<Arguments> testArguments(){
        return StreamOfArguments.get();
    }

}
