package com.ferdonof.capitole.api.controllers;

import com.ferdonof.capitole.api.dtos.ResponsePriceDto;
import com.ferdonof.capitole.business.domains.ResponsePriceBO;
import com.ferdonof.capitole.business.exceptions.PriceNotFoundException;
import com.ferdonof.capitole.business.exceptions.UnhandledPortException;
import com.ferdonof.capitole.business.usecases.GetPriceUseCase;
import com.ferdonof.capitole.port.PriceEntityPort;
import com.ferdonof.capitole.utils.StreamOfArguments;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Import(WebTestConfiguration.class)
@WebMvcTest( PricesController.class)
class PricesControllerTest {
    @MockBean
    private GetPriceUseCase getPriceUseCase;
    @Mock
    private PriceEntityPort priceEntityPort;

    @Autowired
    private MockMvc mockMvc;

    private LocalDateTime now;
    private MultiValueMap<String, String> params = new LinkedMultiValueMap<>();

    @BeforeEach
    void beforeEach(){
        now = LocalDateTime.now();
        params.add("product_id", "35455");
        params.add("brand_id", "1");
        params.add("date", now.toString());
    }



    @ParameterizedTest
    @MethodSource("testArguments")
    void whenRequestIsValidAndEntityExistsThenReturnResponsePriceDto(LocalDateTime date, Long brandId, Long productId, ResponsePriceDto response) throws Exception {
        when(getPriceUseCase.execute(date, brandId, productId)).thenReturn(ResponsePriceBO
                .builder()
                .brandId(brandId)
                .productId(productId)
                .startDate(response.getStartDate())
                .endDate(response.getEndDate())
                .rateId(response.getRateId())
                .price(response.getPrice())
                .build());

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("product_id", "35455");
        params.add("brand_id", "1");
        params.add("date", date.toString());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/prices")
                        .params(params))
                        .andDo(print()).andExpect(status().isOk())
                        .andExpect(jsonPath("$.product_id").value(response.getProductId()))
                        .andExpect(jsonPath("$.brand_id").value(response.getBrandId()))
                        .andExpect(jsonPath("$.rate_id").value(response.getRateId()))
                        .andExpect(jsonPath("$.price").value(response.getPrice()))
                        .andExpect(jsonPath("$.end_date").value(response.getEndDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"))))
                        .andExpect(jsonPath("$.start_date").value(response.getStartDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"))))
                        .andReturn();

        verify(getPriceUseCase, times(1)).execute(date, brandId, productId);
    }

    @Test
    void whenRequestIsValidButEntityNotExistsShouldReturnPriceNotFoundException() throws Exception {
        when(getPriceUseCase.execute(now, 1l, 35455l)).thenThrow(new PriceNotFoundException("Can't find price for "));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/prices")
                        .params(params))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue((result.getResolvedException() instanceof PriceNotFoundException)))
                .andExpect(result -> Assertions.assertEquals("Can't find price for ", result.getResolvedException().getMessage()));

        verify(getPriceUseCase, times(1)).execute(now, 1l, 35455l);
    }


    @Test
    void whenRequestIsValidWithUnhandledErrorShouldReturnUnhandledPortException() throws Exception {
        when(getPriceUseCase.execute(now, 1l, 35455l)).thenThrow(new UnhandledPortException("Unhandled exception"));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/prices")
                        .params(params))
                .andDo(print())
                .andExpect(status().isInternalServerError())
                .andExpect(result -> Assertions.assertTrue((result.getResolvedException() instanceof UnhandledPortException)))
                .andExpect(result -> Assertions.assertEquals("Unhandled exception", result.getResolvedException().getMessage()));

        verify(getPriceUseCase, times(1)).execute(now, 1l, 35455l);
    }

    @Test
    void whenIsMissingBrandIdInRequestShouldReturnBadRequest() throws Exception {
        params.remove("brand_id");
        mockMvc.perform(MockMvcRequestBuilders.get("/api/prices")
                        .params(params))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(result -> result.getResolvedException().getLocalizedMessage().contains("entity.not.found"));
    }

    @Test
    void whenIsMissingProductIdInRequestShouldReturnBadRequest() throws Exception {
        params.remove("product_id");
        mockMvc.perform(MockMvcRequestBuilders.get("/api/prices")
                        .params(params))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(result -> result.getResolvedException().getLocalizedMessage().contains("entity.not.found"));;
    }

    @Test
    void whenIsMissingDateInRequestShouldReturnBadRequest() throws Exception {

        params.remove("date");

        mockMvc.perform(MockMvcRequestBuilders.get("/api/prices")
                        .params(params))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(result -> result.getResolvedException().getLocalizedMessage().contains("entity.not.found"));
    }


    private static Stream<Arguments> testArguments(){
        return StreamOfArguments.get();
    }
}