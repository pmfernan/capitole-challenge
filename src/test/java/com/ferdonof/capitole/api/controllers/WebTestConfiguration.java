package com.ferdonof.capitole.api.controllers;

import com.ferdonof.capitole.business.usecases.GetPriceUseCase;
import com.ferdonof.capitole.business.usecases.impl.GetPriceUseCaseImpl;
import com.ferdonof.capitole.port.PriceEntityPort;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class WebTestConfiguration {

    @MockBean
    PriceEntityPort priceEntityPort;

    @Bean
    public GetPriceUseCase getPriceUseCase(){
        return new GetPriceUseCaseImpl(priceEntityPort);
    }
}
