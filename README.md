# Capitole code challenge

## Enunciado
En la base de datos de comercio electrónico de la compañía disponemos de la tabla PRICES que refleja el precio final (pvp) y la tarifa que aplica a un producto de una cadena entre unas fechas determinadas. A continuación se muestra un ejemplo de la tabla con los campos relevantes:

#### PRICES

|BRAND_ID | START_DATE          | END_DATE            | PRICE_LIST   | PRODUCT_ID | PRIORITY | PRICE | CURR  |
|---------|---------------------|---------------------|--------------|------------|----------|-------|-------|
|1        | 2020-06-14-00.00.00 | 2020-12-31-23.59.59 | 1            | 35455      | 0        | 35.50 | EUR   |
|1        | 2020-06-14-15.00.00 | 2020-06-14-18.30.00 | 2            | 35455      | 1        | 25.45 | EUR   |
|1        | 2020-06-15-00.00.00 | 2020-06-15-11.00.00 | 3            | 35455      | 1        | 30.50 | EUR   |
|1        | 2020-06-15-16.00.00 | 2020-12-31-23.59.59 | 4            | 35455      | 1        | 38.95 | EUR   |

Campos:

- **BRAND_ID**: foreign key de la cadena del grupo (1 = ZARA).
- **START_DATE , END_DATE**: rango de fechas en el que aplica el precio tarifa indicado.
- **PRICE_LIST**: Identificador de la tarifa de precios aplicable.
- **PRODUCT_ID**: Identificador código de producto.
- **PRIORITY**: Desambiguador de aplicación de precios. Si dos tarifas coinciden en un rago de fechas se aplica la de mayor prioridad (mayor valor numérico).
- **PRICE**: precio final de venta.
- **CURR**: iso de la moneda.

Se pide:

Construir una aplicación/servicio en SpringBoot que provea una end point rest de consulta  tal que:

Acepte como parámetros de entrada: fecha de aplicación, identificador de producto, identificador de cadena.
Devuelva como datos de salida: identificador de producto, identificador de cadena, tarifa a aplicar, fechas de aplicación y precio final a aplicar.

Se debe utilizar una base de datos en memoria (tipo h2) e inicializar con los datos del ejemplo, (se pueden cambiar el nombre de los campos y añadir otros nuevos si se quiere, elegir el tipo de dato que se considere adecuado para los mismos).

Desarrollar unos test al endpoint rest que  validen las siguientes peticiones al servicio con los datos del ejemplo:

- *Test 1*: petición a las 10:00 del día 14 del producto 35455   para la brand 1 (ZARA)
- *Test 2*: petición a las 16:00 del día 14 del producto 35455   para la brand 1 (ZARA)
- *Test 3*: petición a las 21:00 del día 14 del producto 35455   para la brand 1 (ZARA)
- *Test 4*: petición a las 10:00 del día 15 del producto 35455   para la brand 1 (ZARA)
- *Test 5*: petición a las 21:00 del día 16 del producto 35455   para la brand 1 (ZARA)


Se valorará:

- Diseño y construcción del servicio.
- Calidad de Código.
- Resultados correctos en los test.

## Elección de la arquitectura 

Opté por la implementación de una arquitectura limpia, una versión de la arquitectura de puertos y adaptadores, en la cual se separa el dominio de negocio de la infraestructura. Hay mucha documentación extendida respecto de este y otros tipos de diseño de aplicación, muy convenientes a la hora de pensar el escalado de las aplicaciones, y facilita mucho el desarrollo y el entendimiento de todas las caps del sistema.

## Configuración del proyecto
La base de datos se configura en el archivo *application.properties* del directorio **resources**. Las variables se encuentran expuestas, ya que es un proyecto pequeño y solo para ser evaluado, pero en un proyecto productivo es conveniente mapear los valores de las variables y los secrets a variables de entorno, que puedan ser almacenadas en repositorios seguros, como por ejemplo Hashicorp Vault, o mapeadas en entornos de Openshift, o haciendo uso de un servidor de configuración.

Como indica el enunciado, utilicé una base de datos H2 en memoria, y cargué la tabla y los datos los datos básicos que se mencionan en el enunciado dpor medio del archivo **data.sql** ubicado en el paquete **resources**.
Si bien se estila separar los datos de la creación de los objetos de la base de datos (en un archivo data.sql y schema.sql, respectivamente), al ser pocos datos y tener solamente una tabla preferí dejarlos en el mismo archivo.
No se cambiaron los nombres de los campos de la base de datos, ya que consideré que podría tratarse de una BD Legacy, y de esa manera no sería posible hacerlo, o quizá demasiado costoso.


El puerto en el que se ejecuta la aplicación es el puerto por defecto  de tomcat (8080).

## Tests

Para el desarrollo de los tests utilicé Mokito y MockMvc. Este último no lo había utilizado nunca, por lo que quise probar su funcionamiento y si bien es limitado pude realizar la mayor cobertura posible de los tests.
Realicé tests unitarios y de integración.

## Swagger

Se disponibiliza en la dirección [http://localhost:8080/swagger-ui/index.html](http://localhost:8080/swagger-ui/index.html) para obtener la información básica del servicio. 

## Postman

En el directorio **extra**, dejé una colección postman con una request de ejemplo para que pueda testearse el proyecto.
Se puede descargar desde [aquí](https://gitlab.com/pmfernan/capitole-challenge/-/tree/main/extra/Capitole.postman_collection.json)

# Actualización domingo 24/7

Motivado por el desarrollo de una versión reactiva del proyecto, realicé los cambios necesarios, partiendo de la que ya tenía (respetando la arquitectura de la aplicación original) y utilizando **Spring webflux**.
Esta versión está subida al branch [***reactive***](https://gitlab.com/pmfernan/capitole-challenge/-/tree/reactive) , y permite ejecutar y obtener los mismos resultados que con la versión original.
Para investigar como funciona el paradigma me he valido de varias fuentes, entre las cuales se encuentran:

- La documentación oficial de Spring Boot
- [Spring WebFlux By Example](https://hantsy.github.io/spring-reactive-sample/)
- [Spring Boot. API Rest. Programación reactiva. WebFlux. Introducción](https://www.youtube.com/watch?v=i0lJZeLdAi8&t=21s)
- [[Meetup] Programación Reactiva con Spring WebFlux](https://www.youtube.com/watch?v=SCKJJuSx-ZU&t=1665s)
- [Accessing data with R2DBC](https://spring.io/guides/gs/accessing-data-r2dbc/)
- [Guide to Spring 5 WebFlux](https://www.baeldung.com/spring-webflux)

Desarrollé los test de unidad (utilizando Mockito y WebTestClient)y funcionales utilizando WebTestClient. 
